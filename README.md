# least-squares-curve-fitting
##Curve fitting with linear and nonlinear least squares for Spring 2015 MA 428 Project 2

### Current Build Status
[![PDF Status](https://www.sharelatex.com/github/repos/danielunderwood/least-squares-curve-fitting/builds/latest/badge.svg)](https://www.sharelatex.com/github/repos/danielunderwood/least-squares-curve-fitting/builds/latest/output.pdf)

This code was written for the second project in Spring 2015 MA 428 at North Carolina State University. It explores least-squares in both a linear curve fitting application as well as a nonlinear GPS application.
